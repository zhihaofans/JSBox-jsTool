class Comic {
    constructor() {
        this.CHECK_IN = "comic-universal/802 CFNetwork/1125.2 Darwin/19.4.0 os/ios model/iPhone 11 mobi_app/iphone_comic osVer/13.4 network/2";
    }
}
class User {
    constructor() {
        this.APP_IPHONE = "bili-universal/9320 CFNetwork/1125.2 Darwin/19.5.0 os/ios model/iPhone 11 mobi_app/iphone osVer/13.4.5 network/1";
    }
}
class Common {
    constructor() {
        this.KAAASS = "JSBox-jsTool/0.1 (github:zhuangzhihao-io) <zhuang@zhihao.io>"
    }
}
module.exports = {
    Comic,
    Common,
    User
};